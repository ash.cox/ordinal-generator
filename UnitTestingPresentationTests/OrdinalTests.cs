using System;
using UnitTestingPresentation;
using Xunit;

namespace UnitTestingPresentationTests
{
    public class OrdinalTests
    {

        #region ToOrdinal

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        public void ToOrdinal_EmptyString_ThrowsArgumentException(string value)
        {
            // Arrange / Act / Assert
            Assert.Throws<ArgumentException>(() => Ordinal.ToOrdinal(value));

        }

        [Fact]
        public void ToOrdinal_InvalidArgument_ThrowsArgumentException()
        {
            // Arrange
            var argument = "fish";

            // Act / Assert
            Assert.Throws<FormatException>(() => Ordinal.ToOrdinal(argument));
        }

        [Fact]
        public void ToOrdinal_ValidArgument_ReturnsExpectedResult()
        {
            // Arrange
            var argument = 420;
            var expected = "th";

            // Act
            var result = Ordinal.ToOrdinal(argument.ToString());

            // Assert
            Assert.Equal(expected, result);


        }

        #endregion

        #region GetSuffix

        [Fact]
        public void GetSuffix_EndInOneAndNotEleven_ReturnSt()
        {
            // Arrange
            var argument = 1;
            var expected = "st";

            // Act
            var result = Ordinal.GetSuffix(argument);

            // Assert
            Assert.Equal(expected, result);
        }


        [Fact]
        public void GetSuffix_EndInTwoAndNotTwelve_ReturnsNd()
        {
            // Arrange
            var argument = 2;
            var expected = "nd";

            // Act
            var result = Ordinal.GetSuffix(argument);

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void GetSuffix_EndInThreeAndNotThirteen_ReturnsRd()
        {
            // Arrange
            var argument = 3;
            var expected = "rd";

            // Act
            var result = Ordinal.GetSuffix(argument);

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void GetSuffix_ElevenPassed_ReturnsTh()
        {
            // Arrange
            var argument = 11;
            var expected = "th";

            // Act
            var result = Ordinal.GetSuffix(argument);

            // Assert
            Assert.Equal(expected, result);

        }

        [Fact]
        public void GetSuffix_TwelvePassed_ReturnsTh()
        {
            // Arrange
            var argument = 12;
            var expected = "th";

            // Act
            var result = Ordinal.GetSuffix(argument);

            // Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void GetSuffix_ThirteenPassed_ReturnsTh()
        {
            // Arrange
            var argument = 13;
            var expected = "th";

            // Act
            var result = Ordinal.GetSuffix(argument);

            // Assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("1146")]
        [InlineData("9568")]
        [InlineData("55")]
        [InlineData("15")]
        [InlineData("4")]

        public void GetSuffix_AnyOtherNumberOtherElevenTwelveThirteenOrEndingInOneTwoThree_ReturnsTh(string value)
        {
            // Arrange
            var argument = Int32.Parse(value);
            var expected = "th";

            // Act
            var result = Ordinal.GetSuffix(argument);

            // Assert
            Assert.Equal(expected, result);
        }

        #endregion

    }
}
