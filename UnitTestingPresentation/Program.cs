﻿using System;

namespace UnitTestingPresentation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(@"
 __________________________________
||O |||R |||D |||I |||N |||A |||L ||
||__|||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|/__\|
");
            Console.WriteLine();

            RunOrdinal();
        }

        private static void RunOrdinal()
        {
            Console.WriteLine();
            Console.WriteLine("Please enter a number to determine it's ordinal suffix...");

            var number = Console.ReadLine();
            var ordinal = Ordinal.ToOrdinal(number);

            Console.WriteLine("---------------------------------------------------");

            WriteDelayedMessageToTerminal("Thinking about it");

            WriteDelayedMessageToTerminal("Crunching the numbers");

            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine($"The ordinal generator has determined the following:");
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));

            Console.WriteLine();
            Console.WriteLine($"[{number}] has the ordinal suffix of [{ordinal}] | [{number}{ordinal}]");
            Console.WriteLine();
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
            Console.WriteLine("---------------------------------------------------");

            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));

            Console.WriteLine();
            Console.WriteLine("Do you want to use the Ordinal Generator again? (y = yes)");

            var doAgain = Console.ReadLine();

            if (doAgain.ToLower() == "y")
            {
                RunOrdinal();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Thank you for using the Ordinal Generator.");
                Console.ReadKey();
            }

        }

        private static void WriteDelayedMessageToTerminal(string message)
        {
            Console.Write(message);
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(0.5));

            Console.Write(".");
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(0.5));

            Console.Write(".");
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(0.5));

            Console.Write(".");
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(0.5));

            Console.Write(" \u2713");
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(0.5));

            Console.WriteLine();
        }
    }
}
