﻿using System;
using System.Text.RegularExpressions;

namespace UnitTestingPresentation
{
    public static class Ordinal
    {
        public static string ToOrdinal(string inVal)
        {
            // Check for empty string
            if (inVal.Trim() == "")
            {
                throw new ArgumentException("Did you forget to enter a number?");
            }

            // Try to parse as in and return value, else throw exception
            var number = Int64.Parse(inVal);

            return GetSuffix(number);

        }

        public static string GetSuffix(long number)
        {
            var lastNumber = Regex.Match(number.ToString(), "\\d{1}$");
            var lastTwoNumbers = Regex.Match(number.ToString(), "\\d{2}$");

            // Ends in 1 and not 11
            if (lastNumber.Value == "1" && lastTwoNumbers.Value != "11")
            {
                return "st";
            }

            // Ends in 2 and not 12
            if (lastNumber.Value == "2" && lastTwoNumbers.Value != "12")
            {
                return "nd";
            }

            // Ends in 3 and note 13
            if (lastNumber.Value == "3" && lastTwoNumbers.Value != "13")
            {
                return "rd";
            }


            return "th";
        }
    }
}